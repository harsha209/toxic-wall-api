var helper = {};

helper.randomString = function(maxLength){
	var text = "";
    
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for(var i=0; i<maxLength; i++ )
    {
    	text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

module.exports = helper;