var express = require('express');

var bodyParser = require('body-parser');

var mongoose = require('mongoose');

var config = require('./config/main');

var app = express();

var allowCrossDomain = function(req, res, next) { //middleware to allow cross domain requests
    res.header('Access-Control-Allow-Origin', "*");

    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');

    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
};

mongoose.connect('mongodb://'+config.dbUser+':'+config.dbPassword+'@127.0.0.1:27017/'+config.dbName);

app.use(bodyParser.json({limit:'50mb'})); //to read data in the body of POST requests, etc

app.use(allowCrossDomain);

app.use(express.static(__dirname+'/static'));

app.use(require('./controllers')); //loading controllers

app.listen(config.port, function(){ //starting server
	console.log('Server running on port '+config.port);
});