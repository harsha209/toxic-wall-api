var mongoose = require('mongoose');

var schema = mongoose.Schema;

var postSchema = new schema({
	names: [String],
	region: String,
	desc: String,
	images: [String],
	date: Date
});

var post = mongoose.model('post', postSchema);

module.exports = post;