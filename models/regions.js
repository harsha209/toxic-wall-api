var mongoose = require('mongoose');

var schema = mongoose.Schema;

var regionSchema = new schema({
	name: String
});

var region = mongoose.model('region', regionSchema);

module.exports = region;