var mongoose = require('mongoose');

var schema = mongoose.Schema;

var commentSchema = new schema({
	postId: String,
	content: String,
	date: Date
});

var comment = mongoose.model('comment', commentSchema);

module.exports = comment;