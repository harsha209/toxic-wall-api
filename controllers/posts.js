var express = require('express');

var helper = require('../helpers/main.js');

var postModel = require('../models/posts');

var regionModel = require('../models/regions');

var router = express.Router();

router.get('/:limit/:offset', function(req, res){ //get multiple posts with limit and offset sorted with latest at the top
	postModel.find().select('names region desc date images').skip(req.params.offset).limit(req.params.limit).sort({date:'desc'}).exec(function(err, posts){
		regionModel.find().exec(function(error, regions){
			posts.forEach(function(post){
				regions.forEach(function(region){
					if(post.region==region.id)
					{
						post.region = region.name;
					}
				});
			});

			res.json({
			success:true,
			posts:posts
		}, 200);
		});
	});
});

router.get('/:postId', function(req, res){ //get details of a particular post c
	postModel.findById(req.params.postId, function(err, post){
		regionModel.find().exec(function(err, regions){
			regions.forEach(function(region){
				if(region._id==post.region)
				{
					post.region = region.name;
				}
			})

			res.json({
				success:true,
				_id:req.params.postId,
				names:post.names,
				region:post.region,
				desc:post.desc,
				date:post.date,
				images:post.images
			}, 200);
		})
	});
});

router.post('/', function(req, res){ //add a new post
	var fs = require('fs');

	var images = req.body.images;

	var imageNames = [], imageName;

	images.forEach(function(image){
		imageName = helper.randomString(8)+'.jpg';

		fs.writeFile(__dirname+'/static/images/'+imageName, image, 'base64', function(err){
			if(err)
			{
				throw err;
			}
		});

		imageNames.push(imageName);
	});

	var post = new postModel({
		names: req.body.names,
		region: req.body.region,
		desc: req.body.desc,
		images: imageNames,
		date: new Date()
	});

	post.save(function(err, newPost){
		if(err) throw err;

		res.json({success:true, postId:newPost.id}, 200);
	});
});

module.exports = router;