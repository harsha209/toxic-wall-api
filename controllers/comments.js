var express = require('express');

var router = express.Router();

var commentModel = require('../models/comments');

router.get('/:postId/:limit/:offset', function(req, res){
	commentModel.find({postId:req.params.postId}).select('content date').skip(req.params.offset).limit(req.params.limit).sort({date:'desc'}).exec(function(err, comments){
		res.json({
			success: true,
			comments: comments
		}, 200);
	});
});

router.post('/', function(req, res){
	var date = new Date();

	var comment = new commentModel({
		postId: req.body.postId,
		content: req.body.content,
		date: date
	});

	comment.save(function(err, newComment){
		res.json({
			success: true,
			date: date
		}, 200);
	});
});

module.exports = router;