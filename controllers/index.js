var express = require('express');

var router = express.Router();

router.use('/post', require('./posts')); //use posts controller for requests starting with /post

router.use('/comment', require('./comments')); //use comments controller for requests starting with /comment

router.use('/region', require('./regions')); //use servers controller for requests starting with /server

router.get('/', function(req, res){ //when base url is called
	res.send('Toxic Wall Api');
});

module.exports = router;