var express = require('express');

var router = express.Router();

var regionModel = require('../models/regions');

router.get('/', function(req, res){
	regionModel.find().select('_id name').exec(function(err, regions){
		response = {
			success:true,
			regions:regions
		};

		res.json(response);
	});
});

module.exports = router;